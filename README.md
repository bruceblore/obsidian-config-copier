# Obsidian Config Copier
A simple script to copy the configuration from a designated "main" vault into a new Obsidian vault

## Running
Eventually, I plan to have a way to install this to $PATH as `obs-cpy` (as reflected in the help output), but for now, you still need to type the full path of wherever you cloned this to.

The simplest way to use the program is to `cd` into the directory of your new vault. Do not manually create a .obsidian folder, and do not `cd` into an existing `.obsidian` folder. Run the program without arguments, and the configuration from your default vault, which by default is `~/Obsidian/Main` will be copied into the current working directory.

### Other options
```
usage: obs-copy [-h] [-m MAIN] [-t TARGET] [--excludeFiles [EXCLUDEFILES ...]] [--includeFiles [INCLUDEFILES ...]] [--excludePlugins [EXCLUDEPLUGINS ...]]
                [--includePlugins [INCLUDEPLUGINS ...]] [-d]

A simple script to copy the configuration from a designated "main" vault into a new Obsidian vault

options:
  -h, --help            show this help message and exit
  -m MAIN, --main MAIN  The main vault to copy your config from. Default is ~/Obsidian/Main.
  -t TARGET, --target TARGET
                        The target vault to copy your config to. Default is current working directory.
  --excludeFiles [EXCLUDEFILES ...]
                        Additional files to exclude, beyond the configured ones: daily-notes.json, graph.json, page-preview.json, workspace.json, workspace-mobile.json,
                        core-plugins-migration.json
  --includeFiles [INCLUDEFILES ...]
                        Files to include, even if they are on the exclude list
  --excludePlugins [EXCLUDEPLUGINS ...]
                        Additional plugins to exclude, beyond the configured ones: calendar, obsidian-rollover-daily-todos
  --includePlugins [INCLUDEPLUGINS ...]
                        Plugins to include, even if they are on the exclude list
  -d, --dry-run         Print the operations that would be performed, but do not actually perform them
```

## Configuration
The config file is `~/.config/obs-cpy/config.json`. If it does not exist, the program will attempt to create it with a default configuration. The default configuration is below. This configuration is intended to be appropriate if you use the daily notes in your main vault as a journal and/or planner, and do not use daily notes in your other vaults.
```jsonc
{
    // The default vault to copy from, if -m or --main are not specified on the command line.
    "mainVault": "~/Obsidian/Main",

    // Files to exclude from the copy by default. You probably do not need to modify this, other than possibly removing `daily-notes.json`.
    "excludeFiles": [
        "daily-notes.json",                 // Configuration and data related to your daily notes.
        "graph.json",                       // Data related to the graph view.
        "page-preview.json",                // Data related to previews.
        "workspace.json",                   // What you have open and how you have them arranged, on your desktop/laptop
        "workspace-mobile.json",            // Same as above, but on your phone/tablet
        "core-plugins-migration.json",      // Internal file for tracking whether plugins have been upgraded
    ],

    // List of files to be given special treatment as plugin files. Instead of being copied, they will be rewritten, with the plugins configured below omitted. You almost certainly do not need to modify this.
    "pluginFiles": ["community-plugins.json", "core-plugins.json"],

    // List of plugins to disable. Note that the plugins and their configuration will still be copied. They will simply be omitted from the pluginFiles listed above, resulting in them being installed but disabled.
    "excludePlugins": ["calendar", "obsidian-rollover-daily-todos"]
}
```