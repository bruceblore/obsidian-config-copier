#!/usr/bin/env python
import os
import shutil
from xdg_base_dirs import xdg_config_home  # TODO add python-xdg-base-dirs dependency
import argparse
import json

# Load the configuration, and write the config file if one does not exist yet
CONFIG_PATH = os.path.join(xdg_config_home(), "obs-copy", "config.json")
OBSIDIAN_FOLDER_NAME = ".obsidian"

config = {
    "mainVault": "~/Obsidian/Main",
    "excludeFiles": [
        "daily-notes.json",
        "graph.json",
        "page-preview.json",
        "workspace.json",
        "workspace-mobile.json",
        "core-plugins-migration.json",
    ],
    "pluginFiles": ["community-plugins.json", "core-plugins.json"],
    "excludePlugins": ["calendar", "obsidian-rollover-daily-todos"],
}

if os.path.isfile(CONFIG_PATH):
    try:
        with open(CONFIG_PATH) as configFile:
            config.update(json.JSONDecoder().decode(configFile.read()))
    except json.JSONDecodeError:
        print(f"Error reading config file at {CONFIG_PATH}: Invalid JSON")
        raise SystemExit(1)
    except Exception as e:
        print(f"Error reading config file at {CONFIG_PATH}: {e}")
else:
    try:
        os.makedirs(os.path.dirname(CONFIG_PATH), exist_ok=True)
        with open(CONFIG_PATH, "w") as configFile:
            json.dump(config, configFile, indent=4)
    except Exception as e:
        print(f"Error writing config file to {CONFIG_PATH}: {e}")
        print(f"Program execution will continue")

# Parse the command line options
parser = argparse.ArgumentParser(
    prog="obs-copy",
    description='A simple script to copy the configuration from a designated "main" vault into a new Obsidian vault',
    epilog="Developed by Bruce Blore: https://gitlab.com/bruceblore/obsidian-config-copier",
)
parser.add_argument(
    "-m",
    "--main",
    help=f"The main vault to copy your config from. Default is {config['mainVault']}.",
    default=config["mainVault"],
)
parser.add_argument(
    "-t",
    "--target",
    help="The target vault to copy your config to. Default is current working directory.",
    default=os.getcwd(),
)
parser.add_argument(
    "--excludeFiles",
    help=f"Additional files to exclude, beyond the configured ones: {', '.join(config['excludeFiles'])}",
    nargs="*",
    default=[],
)
parser.add_argument(
    "--includeFiles",
    help="Files to include, even if they are on the exclude list",
    nargs="*",
    default=[],
)
parser.add_argument(
    "--excludePlugins",
    help=f"Additional plugins to exclude, beyond the configured ones: {', '.join(config['excludePlugins'])}",
    default=[],
    nargs="*",
)
parser.add_argument(
    "--includePlugins",
    help="Plugins to include, even if they are on the exclude list",
    default=[],
    nargs="*",
)
parser.add_argument(
    "-d",
    "--dry-run",
    help="Print the operations that would be performed, but do not actually perform them",
    action="store_true",
)

args = parser.parse_args()


# Build up a list of files to copy
def applyIncludeExclude(initial: list, include: list, exclude: list) -> list:
    """Apply include and exclude lists to an initial list.

    First, remove everything from the initial list that also appears in the exclude list. Then, add everything that appears in the include list that isn't already in the initial list. If an item is in both the include and exclude lists, it is included.

    Args:
        initial (list): The list to start with
        include (list): The list of all items that must be included
        exclude (list): The list of all items that must not be included, unless they are in the include list

    Returns:
        list: The modified list
    """
    for toExclude in exclude:
        if toExclude in initial:
            initial.remove(toExclude)
    for toInclude in include:
        if toInclude not in initial:
            initial.append(toInclude)
    return initial


sourceDir = os.path.expanduser(os.path.join(args.main, OBSIDIAN_FOLDER_NAME))
filesToCopy = []
for path, dirs, files in os.walk(sourceDir):
    for file in files:
        filesToCopy.append(os.path.relpath(os.path.join(path, file), sourceDir))
config["excludeFiles"].extend(args.excludeFiles)
config["excludeFiles"].extend(
    config["pluginFiles"]
)  # Plugin files are excluded because they are handled specially
filesToCopy = applyIncludeExclude(
    filesToCopy, args.includeFiles, config["excludeFiles"]
)

# Copy the files
targetDir = os.path.join(os.path.expanduser(args.target), OBSIDIAN_FOLDER_NAME)
for file in filesToCopy:
    source = os.path.join(
        sourceDir, file
    )  # I know I can condense this into fewer lines, but I want to be extra sure that the dry run option is accurate
    target = os.path.join(targetDir, file)
    if args.dry_run:
        print(f"Copy {source} to {target}")
    else:
        os.makedirs(os.path.dirname(target), exist_ok=True)
        shutil.copy(source, target)

# Enable plugins
config["excludePlugins"].extend(args.excludePlugins)
for pluginFileName in config["pluginFiles"]:
    try:
        with open(os.path.join(sourceDir, pluginFileName)) as pluginFile:
            enabledPlugins = json.JSONDecoder().decode(pluginFile.read())
    except json.JSONDecodeError:
        print(f"Error reading enabled plugins from {pluginFileName}: invalid JSON")
        print("Program execution will continue")
    except FileNotFoundError:
        print(
            f"Error reading enabled plugins from {pluginFileName}: file does not exist"
        )
        print("Program execution will continue")
    except Exception as e:
        print(f"Error reading enabled plugins from {pluginFileName}: {e}")
        print("Program execution will continue")

    enabledPlugins = applyIncludeExclude(
        enabledPlugins, args.includePlugins, config["excludePlugins"]
    )

    if args.dry_run:
        print(f'Enable plugins from {pluginFileName}: {", ".join(enabledPlugins)}')
    else:
        target = os.path.join(targetDir, pluginFileName)
        os.makedirs(os.path.dirname(target), exist_ok=True)
        with open(target, "w") as targetFile:
            targetFile.write(json.JSONEncoder().encode(enabledPlugins))
